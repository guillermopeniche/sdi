function validatePass(sPass) {
  var rePass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
  if (!sPass.match(rePass)) {
    alert("Invalid Password. Must have at least 8 characters, 1 number, 1 upper and 1 lowercase ");
    document.getElementById("mySubmit").disabled = true;
    return false;
  }
}
