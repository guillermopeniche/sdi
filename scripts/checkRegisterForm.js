function validateForm() {
    var error = "";
    var sEmail = document.forms["myForm"]["username"].value
    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
    if (!sEmail.match(reEmail)) {
        error = error + "Invalid email address \n \n";
    }

    var sPass = document.forms["myForm"]["password"].value
    var rePass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    if (!sPass.match(rePass)) {
        error = error + "Invalid Password. Must have at least 8 characters, 1 number, 1 upper and 1 lowercase  \n \n";
    }
    var sPhone = document.forms["myForm"]["phone"].value
    var rePhone = /\d{10}/;
    if (!sPhone.match(rePhone)) {
        error = error + "Invalid Phone number. Must have 10 digits \n";
    }
    if (error.length >= 1) {
        alert(error);
        return false;
    }
}